using UnityEngine;

public class LevelSceneUIService : MonoBehaviour
{
    [SerializeField] GameObject levelCanvas, gameOverCanvas, ControlTipsCanvas;

    void Awake()
    {
        Application.targetFrameRate = 60;
        // save = AllManagersReference.SaveData;
        // levelManager = AllManagersReference.LevelManager;
    }
    public void BackToMainMenu(int unloadSceneIndex)
    {
        int loadSceneIndex = 1;
        // AllManagersReference.LevelLoader.LoadLevel(loadSceneIndex, unloadSceneIndex);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void GameOverMenu(bool toggle)
    {
        levelCanvas.SetActive(false);
        gameOverCanvas.SetActive(true);
    }
    public void LevelOptionsMenu(bool toggle)
    {

    }
}
