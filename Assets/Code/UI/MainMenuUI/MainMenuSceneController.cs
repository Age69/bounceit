using UnityEngine;

public class MainMenuSceneController : MonoBehaviour
{
    [SerializeField] GameObject mainMenuButtonCanvas, mainOptionsCanvas, levelCreationCanvas, gameName;

    public void TuneMainMenu()
    {
        gameName.SetActive(true);
        mainMenuButtonCanvas.SetActive(true);
        mainOptionsCanvas.SetActive(false);
        levelCreationCanvas.SetActive(false);
    } 
    public void TuneLevelCreationMenu()
    {
        gameName.SetActive(false);
        mainMenuButtonCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(false);
        levelCreationCanvas.SetActive(true);
    }
    public void TuneOptionsMenu()
    {
        gameName.SetActive(false);
        mainMenuButtonCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(true);
        levelCreationCanvas.SetActive(false);
    } 
    public void QuitGame()
    {
        Application.Quit();
    }
}
