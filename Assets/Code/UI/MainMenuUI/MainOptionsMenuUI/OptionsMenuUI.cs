using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;
using Unity.Mathematics;

public class OptionsMenuUI : MonoBehaviour
{
    [SerializeField] Toggle fullScreenTog, vsyncTog, tutorialTog, soundTog;
    [SerializeField] Slider soundSlider, refrashRateSlider;
    [SerializeField] TMP_Text resolutionValue, refrashRate;
    [SerializeField] Button apllyButton;
    [SerializeField] ResolutionApplyPanel resApplyPanel;
    bool apllyRes, apllyRefrash;
    MainMenuSceneController mainMenuController;
    SettingsSaveService settingsService;
    GameSettingsService gameSettings;
    ResolutionSettingsService resolutionSettings;
    SoundSettingsServise soundSettings;

    [Inject]
    void Contractor(MainMenuSceneController _mainMenuController, SettingsSaveService _settingsSave,
    SoundSettingsServise _soundSettings, GameSettingsService _gameSettings, ResolutionSettingsService _resolutionService)
    {
        mainMenuController = _mainMenuController;
        settingsService = _settingsSave;
        gameSettings =_gameSettings;
        resolutionSettings = _resolutionService;
        soundSettings = _soundSettings;
    }
    void Awake()
    {
        resApplyPanel.gameObject.SetActive(false);
        resApplyPanel.ParentStarter();
        UITune();
    }
    void UITune()
    {
        SetResolutionText(resolutionSettings.ApplySelectedResolution());
        fullScreenTog.SetIsOnWithoutNotify(resolutionSettings.FullScreen);
        vsyncTog.SetIsOnWithoutNotify(resolutionSettings.VSync);
        refrashRate.text = resolutionSettings.RefreshRate.ToString();
        soundTog.SetIsOnWithoutNotify(soundSettings.SoundOn);
        soundSlider.SetValueWithoutNotify(soundSettings.SoundValue);
        tutorialTog.SetIsOnWithoutNotify(gameSettings.TutorialOn);
        refrashRateSlider.enabled = false;
        refrashRateSlider.maxValue = resolutionSettings.RefreshRateMax;
        refrashRateSlider.value = resolutionSettings.RefreshRate;
        refrashRateSlider.enabled = true;
        apllyButton.interactable = false;
    }
    void SetResolutionText(int2 _resText)
    {
        resolutionValue.text = _resText.x.ToString() +" x " +_resText.y.ToString();
    }
    void ApllyButtonInteractable()
    {
        apllyButton.interactable = apllyRes || apllyRefrash;
    }
    public void ApllyPanelResult(bool _confirm)
    {
        if(_confirm)
           SetResolutionText(resolutionSettings.ApplySelectedResolution());
        else
        {
           SetResolutionText(resolutionSettings.ReturnToCurrentResolution());
           refrashRateSlider.SetValueWithoutNotify(resolutionSettings.RefreshRate);
           refrashRate.text = resolutionSettings.RefreshRate.ToString();
        }
        resApplyPanel.gameObject.SetActive(false);
    }

#region UIButtonsMethods
    public void ResolutionChangeButton(int _direction)
    {        
        SetResolutionText(resolutionSettings.ChangeResolution(_direction, ref apllyRes));
        ApllyButtonInteractable();
    }
    public void ResolutionApplyButton()
    {
        resolutionSettings.SetSelectedResolution();
        resApplyPanel.gameObject.SetActive(true);
        resApplyPanel.StartApplyPanel();
    }
    public void RefrashRateSlider(float _value)
    {        
        refrashRateSlider.value = _value;
        refrashRate.text = _value.ToString();
        resolutionSettings.ChangeRefrashRate((int)_value, ref apllyRefrash);
        ApllyButtonInteractable();
    }
    public void VsyncToggle(bool _vSyncOn)
    {
        resolutionSettings.VSync = _vSyncOn;
        vsyncTog.isOn = _vSyncOn;
    }
    public void FullScreenToggle(bool _fullOn)
    {
        resolutionSettings.FullScreen = _fullOn;
        fullScreenTog.isOn = _fullOn;
    }
    public void TutorialToggle(bool _tutorOn)
    {
        gameSettings.TutorialOn = _tutorOn;
        tutorialTog.isOn = _tutorOn;
    }   
    public void SoundToggle(bool _soundOn)
    {
        soundSettings.SoundOn = _soundOn;
        soundTog.isOn = _soundOn;
    }   
    public void SoundSlider(float _value)
    {        
        soundSettings.SoundValue = soundSlider.value;
        soundSlider.value = _value;
    }
    public void SaveOptionsButton()
    {
        gameSettings.SaveSettings();
        resolutionSettings.SaveSettings();
        soundSettings.SaveSettings();
        settingsService.PackSettings();
    }
    public void ResetOptionsButton()
    {
        settingsService.ResetSettingsToDeafult();
        gameSettings.LoadSettings();
        resolutionSettings.LoadSettings();
        soundSettings.LoadSettings();
        UITune();
    }
    public void BackToMainMenuButton()
    {       
        mainMenuController.TuneMainMenu();
    }
#endregion
}
