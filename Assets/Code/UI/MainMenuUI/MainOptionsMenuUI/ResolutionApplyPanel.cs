using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class ResolutionApplyPanel : MonoBehaviour
{
    [SerializeField] float resApplyPanelTimer;
    [SerializeField] Slider applyTimerSlider;
    Coroutine applyPanelCor;
    OptionsMenuUI optionsMenuUI;
    
    public void ParentStarter()
    {
        optionsMenuUI = GetComponentInParent<OptionsMenuUI>();
        applyTimerSlider.maxValue = resApplyPanelTimer;
        applyTimerSlider.value = resApplyPanelTimer;
    }
    public void StartApplyPanel()
    {           
        applyPanelCor = StartCoroutine(PanelTimer());
    }
    public void ApplyPanelConfirmButton()
    {
        StopCoroutine(applyPanelCor);
        DisableApplyPanel(true);     
    }
    void DisableApplyPanel(bool _confirm)
    {
        applyTimerSlider.value = resApplyPanelTimer;
        optionsMenuUI.ApllyPanelResult(_confirm);        
    }
    IEnumerator PanelTimer()
    {
        float timer = Time.unscaledTime + resApplyPanelTimer;
        while (timer > Time.unscaledTime)
        {
            applyTimerSlider.value -= Time.unscaledDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        DisableApplyPanel(false);
    }
}
