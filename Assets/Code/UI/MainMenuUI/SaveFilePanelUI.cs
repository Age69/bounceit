using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Zenject;

public class SaveFilePanelUI : MonoBehaviour
{
   [SerializeField] TMP_InputField inputField;
   [SerializeField] TextMeshProUGUI[] profileButtons;
   int selectedIndex;
   MainMenuUI mainMenuUI;
   ProfileService profileService;
   ApplyPanel applyPanel;
   
   [Inject]
   public void Countractor(ProfileService _profileService, ApplyPanel _applyPanel)
   {
      profileService = _profileService;
      applyPanel = _applyPanel;
   }
   public void ParentStarter()
   {
      inputField.gameObject.SetActive(false);
      mainMenuUI = GetComponentInParent<MainMenuUI>();
      for(int i = 0; i < profileButtons.Length; i++)
      {
         profileButtons[i].text = profileService.ProfileData.Profiles[i].profileName;
      }     
   }
   public void ClosePanelButton()
   {
      inputField.gameObject.SetActive(false);
      profileService.PackProfileData();
      gameObject.SetActive(false);
   }
   public void SaveSlotButton(int _index)
   {      
      if(profileService.ProfileData.Profiles[_index].newGame)
      {
         //new profile
         selectedIndex = _index;       
         inputField.transform.position = profileButtons[_index].transform.position;      
         inputField.gameObject.SetActive(true); 
         inputField.ActivateInputField();
      }
      else
      {
         if(profileService.ProfileData.selectedIndex != _index)
         {
            //change profile
            profileService.SetSelectedProfile(_index);
            profileService.SetSelectedIndex(_index);
            mainMenuUI.SetProfileUI();           
         }
         ClosePanelButton();
      }      
   }
   public void InputProfileName(string _name)
   {
      if(!string.IsNullOrWhiteSpace(_name))
      {
         profileService.SetSelectedProfile(selectedIndex);
         profileService.StartNewProfile(_name);
         profileButtons[selectedIndex].text = _name;
         mainMenuUI.SetProfileUI();
         inputField.text = "";
         ClosePanelButton();
      }
      else
      {         
         inputField.gameObject.SetActive(false);
      }     
   }
   public void DeleteProfileButton(int _index)
   {
      selectedIndex = _index;
      applyPanel.PanelResult += DeleteAproof;
      applyPanel.SetQuestion("Delete Profile ?");
      applyPanel.transform.parent.gameObject.SetActive(true);
   }
   void DeleteAproof(bool _aproof)
   {
      applyPanel.PanelResult -= DeleteAproof;
      applyPanel.transform.parent.gameObject.SetActive(false);
      if(_aproof)
      {
         profileService.DeleteProfile(selectedIndex);
         profileButtons[selectedIndex].text = profileService.ProfileData.Profiles[selectedIndex].profileName;
         if(profileService.ProfileData.selectedIndex == selectedIndex)
         {
            profileService.SetSelectedProfile(selectedIndex);
            profileService.SetSelectedIndex(selectedIndex);           
            mainMenuUI.ClearProfileName();
         }
      }
   }
}
