﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] TMP_Text profileName;
    [SerializeField] SaveFilePanelUI saveFilePanel;
    MainMenuSceneController mainMenuController;
    ProfileService profileService;

    [Inject]
    public void Countractor(MainMenuSceneController _mainMenuService, ProfileService _profileService)
    {
        mainMenuController = _mainMenuService;
        profileService = _profileService;
    }
    void Awake()
    {
        saveFilePanel.gameObject.SetActive(false);
        saveFilePanel.ParentStarter();
        if (profileService.ProfileCurrent.newGame)
            profileName.gameObject.SetActive(false);
        else
            SetProfileUI();
    }
    public void SetProfileUI()
    {
        profileName.text = profileService.ProfileCurrent.profileName;
        profileName.gameObject.SetActive(true);
    }
    public void ClearProfileName()
    {
        profileName.text = "";
        profileName.gameObject.SetActive(false);
    }
    public void StartGameButton()
    {
        mainMenuController.TuneLevelCreationMenu();
    }
    public void SaveFileSelectionButton()
    {
        saveFilePanel.gameObject.SetActive(!saveFilePanel.gameObject.activeSelf);
    }
    public void OptionsMenuButton()
    {
        mainMenuController.TuneOptionsMenu();
    }
    public void QuitGameButton()
    {
        mainMenuController.QuitGame();
    }
}
