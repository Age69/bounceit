using UnityEngine;
using System;
using TMPro;

public class ApplyPanel : MonoBehaviour
{
    public event Action<bool> PanelResult;
    [SerializeField] TMP_Text question;

    public void SetQuestion(string _text)
    {
        question.text = _text;
    }
    public void ResultButton(bool result)
    {
        PanelResult?.Invoke(result);
    }
}
