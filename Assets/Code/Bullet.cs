using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public abstract void Instantiate(Transform gunMuzzle, float speed, int dmg);
}
