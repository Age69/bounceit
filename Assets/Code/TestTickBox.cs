using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TestTickBox : MonoInstaller
{
    [SerializeField] TestBox testBox;
    public override void InstallBindings()
    {
        Container.BindInstance(testBox);
    }
}
