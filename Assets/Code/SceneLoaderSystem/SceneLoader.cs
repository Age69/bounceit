using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] CanvasGroup crossfade;

    public void SceneLoadingProcessStart(int loadSceneIndex, int unloadSceneIndex)
    {
        StartCoroutine(LoadAsynchronously(loadSceneIndex, unloadSceneIndex));
    }
    IEnumerator LoadAsynchronously(int loadSceneIndex, int unloadSceneIndex)
    {
        crossfade.gameObject.SetActive(true);
        while(crossfade.alpha < 1)
        {
            crossfade.alpha += Time.deltaTime;
            yield return null;
        }      

        AsyncOperation operation = SceneManager.UnloadSceneAsync(unloadSceneIndex);
        while (!operation.isDone)
        {
            yield return null;
        }

        operation = SceneManager.LoadSceneAsync(loadSceneIndex, LoadSceneMode.Additive);
        while (!operation.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(loadSceneIndex));

        while(crossfade.alpha > 0)
        {
            crossfade.alpha -= Time.deltaTime;
            yield return null;
        }
        crossfade.gameObject.SetActive(false);
    }
}
