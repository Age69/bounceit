using UnityEngine;
using Zenject;

public class MainMenuStarter : MonoBehaviour
{
    MainMenuSceneController mainMenuController;

    [Inject]
    void Contractor(MainMenuSceneController _mainMenuController)
    {
        mainMenuController = _mainMenuController;
    }
    void Awake()
    {
        mainMenuController.TuneMainMenu();
        Destroy(gameObject);
    }
}
