using UnityEngine;
using Zenject;

public class RootSceneInstaller : MonoInstaller
{
    [SerializeField] SceneLoader sceneLoader;

    [SerializeField] GlobalUpdateSystem oneUpdateSystem;
    [SerializeField] GlobalFixUpdateSystem oneFixUpdateSystem;

    [SerializeField] SaveDataDirectory saveDataDirectory;
    [SerializeField] SettingsSaveService settingsSaveService;
    [SerializeField] ProfileSaveService profileSaveService;
    [SerializeField] ProfileService profileService;
    [SerializeField] GameSettingsService gameSettingsService;
    [SerializeField] ResolutionSettingsService resolutionSettingsService;
    [SerializeField] SoundSettingsServise soundSettingsServise;

    public override void InstallBindings()
    {
        // Container.BindInterfacesAndSelfTo<TestTickBox>().AsSingle().NonLazy();
        // Container.Bind<TestTickBox>().AsSingle().NonLazy();
        Container.Bind<SceneLoader>().FromInstance(sceneLoader).AsSingle().NonLazy();

        Container.Bind<GlobalUpdateSystem>().FromInstance(oneUpdateSystem).AsSingle().NonLazy();
        Container.Bind<GlobalFixUpdateSystem>().FromInstance(oneFixUpdateSystem).AsSingle().NonLazy();

        Container.BindInstance(saveDataDirectory).AsSingle().NonLazy();
        Container.Bind<SettingsSaveService>().FromInstance(settingsSaveService).AsSingle().NonLazy();
        Container.Bind<ProfileSaveService>().FromInstance(profileSaveService).AsSingle().NonLazy();
        Container.Bind<ProfileService>().FromInstance(profileService).AsSingle().NonLazy();
        Container.Bind<GameSettingsService>().FromInstance(gameSettingsService).AsSingle().NonLazy();
        Container.Bind<ResolutionSettingsService>().FromInstance(resolutionSettingsService).AsSingle().NonLazy();
        Container.Bind<SoundSettingsServise>().FromInstance(soundSettingsServise).AsSingle().NonLazy();
        // Container.BindInstance(sceneLoader).AsSingle().NonLazy();
    }
}