using UnityEngine;
using Zenject;

public class MainMenuSceneInstaller : MonoInstaller
{
    [SerializeField] MainMenuSceneController mainMenuSceneController;
    [SerializeField] ApplyPanel applyPanel;
    public override void InstallBindings()
    {
        Container.Bind<MainMenuSceneController>().FromInstance(mainMenuSceneController).AsSingle().NonLazy();
        Container.Bind<ApplyPanel>().FromInstance(applyPanel).AsSingle().NonLazy();
    }
}
