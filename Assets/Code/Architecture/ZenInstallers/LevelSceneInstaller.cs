using UnityEngine;
using Zenject;

public class LevelSceneInstaller : MonoInstaller
{
    [SerializeField] LevelSaveService levelSaveService;
    public override void InstallBindings()
    {
        Container.Bind<LevelSaveService>().FromInstance(levelSaveService).AsSingle().NonLazy();
    }
}
