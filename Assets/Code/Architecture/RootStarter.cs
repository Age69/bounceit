﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Zenject;

public class RootStarter : MonoBehaviour
{
    [SerializeField] CanvasGroup crossfade;
    [SerializeField] int mainMenuSceneIndex;
    SettingsSaveService settingsSave;
    ProfileSaveService profileSave;

    [Inject]
    void Contractor(SettingsSaveService _settingsSave, ProfileSaveService _profileSave)
    {
        settingsSave = _settingsSave;
        profileSave = _profileSave;
    }
    void Awake()
    {
        // StartCoroutine(StartProcess());
        profileSave.RootStarter();
        settingsSave.RootStarter();
    }
    IEnumerator StartProcess()
    {
        crossfade.gameObject.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(mainMenuSceneIndex, LoadSceneMode.Additive);
        while (!operation.isDone)
        {
            yield return null;
        }
        
        while(crossfade.alpha > 0)
        {
            crossfade.alpha -= Time.deltaTime;
            yield return null;
        }
        crossfade.gameObject.SetActive(false);
        SelfDestroy();
    }
    void SelfDestroy()
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }      
}
