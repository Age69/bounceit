using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Parallax : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] GameObject mover;
    [SerializeField] List<RawImage> backImages;
    float lastPositionX;
    float moverOffset;

    void Start()
    {
        lastPositionX = mover.transform.position.x;
        GetComponent<Canvas>().worldCamera = Camera.main;
    }

    void FixedUpdate()
    {
        moverOffset = mover.transform.position.x - lastPositionX;
        foreach(RawImage back in backImages)
        {
            back.uvRect = new Rect(back.uvRect.x - moverOffset * (speed / back.transform.position.z) * Time.deltaTime, 0f, 1f, 1f);
        }
        lastPositionX = mover.transform.position.x;
    }
}
