using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, IDoInFixUpdate
{
    [SerializeField] int piecesInFloorCount;
    [SerializeField] float spawnTimerTime;   
    int centralColumn, totalColumns;
    float xBetweenColumns, spawnTimer;
    List<Vector2> spawnPositions = new List<Vector2>();
    EnemyPoolRandomizer enemyRandomizer;
    LevelSceneUIService levelController;

    void Awake()
    {
        // enemyRandomizer = AllManagersReference.LevelManager.PoolRefManager.EnemyPoolRandomizer;
        // levelController = AllManagersReference.LevelManager.LevelController;
        // totalColumns = AllManagersReference.LevelManager.MapRandomizer.GroupColumns * piecesInFloorCount;
        // xBetweenColumns = AllManagersReference.LevelManager.MapRandomizer.XBetweenEnemy;

        centralColumn = totalColumns / 2;
        if (totalColumns != 0)
        {
            for (int i = 0; i < totalColumns; i++)
            {
                spawnPositions.Add(new Vector2((i - centralColumn) * xBetweenColumns, transform.position.y));
            }
        }
        else
            spawnPositions.Add((Vector2)transform.position);
    }

    void OnEnable()
    {
        // AllManagersReference.FixUpdateController.FixUpdateObjects.Add(this);
    }

    void OnDisable()
    {
        // AllManagersReference.FixUpdateController.FixUpdateObjects.Remove(this);
    }

    void SpawnEnemy()
    {
        int positionIndex = Random.Range(0, spawnPositions.Count);
        /* Enemy enemy = enemyRandomizer.GetRandomEnemy();
        enemy.transform.rotation = transform.rotation;
        enemy.transform.position = spawnPositions[positionIndex];
        enemy.gameObject.SetActive(true); */
    }

    public void DoInFixUpdate()
    {
        if(spawnTimer < Time.time)
        {
            spawnTimer = Time.time + spawnTimerTime;

                SpawnEnemy();
        }
    }
}
