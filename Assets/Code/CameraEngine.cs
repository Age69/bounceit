using UnityEngine;

public class CameraEngine : MonoBehaviour
{
    [SerializeField] float moveSpeed, offsetY;
    float constX, startY, finishY, path, lowestYPos;
    Rigidbody2D rb2;
    bool move;

    void Awake()
    {
        rb2 = GetComponent<Rigidbody2D>();
        constX = rb2.position.x;
        startY = rb2.position.y;
        lowestYPos = startY;
        move = false;
    }

    void LateUpdate()
    {
        if (move)
        {
            path += moveSpeed * Time.deltaTime;
            rb2.MovePosition(new Vector2(constX, Mathf.SmoothStep(startY, finishY, path)));
            if(path > 1)
            {
                move = false;
                path = 0;
                startY = rb2.position.y;
            }
        }
    }
    
    public void SetNewYPosition(float newY)
    {
        if (newY > lowestYPos)
        {
            lowestYPos = newY;
            finishY = newY + offsetY;
            move = true;
        }
    }
}
