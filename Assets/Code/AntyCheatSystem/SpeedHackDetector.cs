﻿using System;
using UnityEngine;

public class SpeedHackDetector : MonoBehaviour
{
    [SerializeField]
    private int life = 3;
    [SerializeField]
    private int gapTolerance = 3;

    private DateTime startDateTime = DateTime.Now;
    private DateTime updatedDateTime = DateTime.Now;

    void Start()
    {
        InvokeRepeating(nameof(checkTime), 0f, 1f);
    }

    void checkTime()
    {
        updatedDateTime = updatedDateTime.AddSeconds(1);
        if (updatedDateTime > DateTime.Now.AddSeconds(gapTolerance))
            updateLife();
    }

    void updateLife()
    {
        life--;
        if (life == 0)
            Application.Quit();

        TimeSpan timeSpan = updatedDateTime - DateTime.Now;
        gapTolerance += Convert.ToInt32(timeSpan.TotalSeconds);
    }
}
