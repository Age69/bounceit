using UnityEngine;
using System;

[Serializable]
public class TestEntitySaveData
{
    public Vector2 testVector;

    public TestEntitySaveData(Vector2 _testVector)
    {
        testVector = _testVector;
    }
}