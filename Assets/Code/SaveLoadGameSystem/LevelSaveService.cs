using UnityEngine;
using System.Linq;
using System;
using Zenject;

public class LevelSaveService : MonoBehaviour
{
    SaveDataDirectory saveDirectory;
    LevelData levelData;
    // DataDirectoryHandler saveLoadSystem;

    // [Inject]
    // void Contractor(DataDirectoryHandler SaveLoadSys)
    // {
    //     saveLoadSystem = SaveLoadSys;
    // }
    public void LoadLevel(string FileName)
    {
        levelData = saveDirectory.UnpackDtaFromFile<LevelData>(FileName, true);

        if (levelData == null)
        {
            NewLevel();
        }
        else
        {
            foreach (ISaveable saveable in FindObjectsOfType<MonoBehaviour>(true).OfType<ISaveable>())
            {
                saveable.LoadFrom(levelData);
            }
        }
    }
    public void SaveLevel(string FileName)
    {
        levelData.ClearData();

        foreach (ISaveable saveable in FindObjectsOfType<MonoBehaviour>(true).OfType<ISaveable>())
        {
            saveable.SaveIn(levelData);
        }
        saveDirectory.PackDataToFile<LevelData>(FileName, levelData, true);
    }
    public void NewLevel()
    {
        levelData = new LevelData();
    }
}
