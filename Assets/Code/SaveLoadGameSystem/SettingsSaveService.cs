using UnityEngine;
using Zenject;

public class SettingsSaveService : MonoBehaviour
{
    [SerializeField] string settingsFileName;
    [SerializeField] SettingsData defaultSettings;
    SettingsData settingsData;
    public SettingsData SettingsData { get => settingsData; }
    SaveDataDirectory saveDirectory;

    [Inject]
    void Contractor(SaveDataDirectory _saveDirectory)
    {
        saveDirectory = _saveDirectory;
    }
    public void RootStarter()
    {
        //tune default settings
        Resolution defaultRes = Screen.resolutions[Screen.resolutions.Length - 1];
        defaultSettings.resolution.x = defaultRes.width;
        defaultSettings.resolution.y = defaultRes.height;
        defaultSettings.refreshRate = defaultRes.refreshRate;
        defaultSettings.refreshRateMax = defaultRes.refreshRate;
        //unpack data
        settingsData = saveDirectory.UnpackDtaFromFile<SettingsData>(settingsFileName, false);
        if (settingsData != null)
            settingsData.refreshRateMax = defaultSettings.refreshRateMax;
        else
            ResetSettingsToDeafult();
    }
    public void PackSettings()
    {
        saveDirectory.PackDataToFile<SettingsData>(settingsFileName, settingsData, false);
    }
    public void ResetSettingsToDeafult()
    {
        settingsData.resolution = defaultSettings.resolution;
        settingsData.fullScreenOn = defaultSettings.fullScreenOn;
        settingsData.refreshRate = defaultSettings.refreshRate;
        settingsData.refreshRateMax = defaultSettings.refreshRateMax;
        settingsData.vSyncOn = defaultSettings.vSyncOn;
        settingsData.tutorialOn = defaultSettings.tutorialOn;
        settingsData.soundOn = defaultSettings.soundOn;
        settingsData.soundValue = defaultSettings.soundValue;
    }
}
