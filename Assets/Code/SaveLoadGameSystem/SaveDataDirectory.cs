using UnityEngine;
using System;
using System.IO;

public class SaveDataDirectory : MonoBehaviour
{
    [Header("Save files directory")]
    [SerializeField] string directoryPath;
    string codeWord = "tarologibemqn";

    public void ParantAwake()
    {
        if (String.IsNullOrWhiteSpace(directoryPath))
            directoryPath = Application.persistentDataPath;
    }
    public void PackDataToFile<T>(string _dataFileName, T _data, bool _encrypted) where T : class
    {
        //different OC have different path separators
        string fullPath = Path.Combine(directoryPath, _dataFileName);
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));

            string jsonData = JsonUtility.ToJson(_data, true);

            if (_encrypted)
                jsonData = EncryptDecrypt(jsonData);

            using (FileStream stream = new FileStream(fullPath, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(jsonData);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("trying to save data to file" + fullPath + "\n" + e);
        }
    }
    public T UnpackDtaFromFile<T>(string _dataFileName, bool _encrypted) where T : class
    {
        //different OC have different path separators
        string fullPath = Path.Combine(directoryPath, _dataFileName);
        T loadedData = null;
        if (File.Exists(fullPath))
        {
            try
            {
                string dataToLoad = "";
                using (FileStream stream = new FileStream(fullPath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        dataToLoad = reader.ReadToEnd();
                    }
                }

                if (_encrypted)
                    dataToLoad = EncryptDecrypt(dataToLoad);

                loadedData = JsonUtility.FromJson<T>(dataToLoad);
            }
            catch (Exception e)
            {
                Debug.LogError("trying to load data from file" + fullPath + "\n" + e);
            }
        }
        return loadedData;
    }
    string EncryptDecrypt(string _data)
    {
        string modifiedData = "";
        for (int i = 0; i < _data.Length; i++)
        {
            modifiedData += (char)(_data[i] ^ codeWord[i % codeWord.Length]);
        }
        return modifiedData;
    }
}
