public interface ISaveable 
{
    void SaveIn(LevelData data);
    void LoadFrom(LevelData data);
}
