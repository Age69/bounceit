using UnityEngine;
using Zenject;

public class ProfileSaveService : MonoBehaviour
{
    [SerializeField] string profileFileName;
    ProfileData profileData;
    public ProfileData ProfileData { get => profileData; }
    SaveDataDirectory saveDirectory;

    [Inject]
    void Contractor(SaveDataDirectory _saveDirectory)
    {
        saveDirectory = _saveDirectory;
    }
    public void RootStarter()
    {
        //unpack data
        profileData = saveDirectory.UnpackDtaFromFile<ProfileData>(profileFileName, false);
        if (profileData == null)
        {
            profileData = new ProfileData();
            for (int i = 0; i < profileData.Profiles.Length; i++)
                profileData.Profiles[i] = new Profile();
        }
    }
    public void PackProfile()
    {
        saveDirectory.PackDataToFile<ProfileData>(profileFileName, profileData, false);
    }
}
