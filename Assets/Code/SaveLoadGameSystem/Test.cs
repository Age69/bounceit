using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Test : MonoBehaviour, ISaveable
{   
    public int testInt;
    public Vector2 testVector = new Vector2();

    void Awake() 
    {
        Debug.Log("awake");
    }
    void Start() 
    {
        Debug.Log("start");
    }

    public void LoadFrom(LevelData saveData)
    {
        if(saveData.testObjStack[0] != null)
        {
            testVector = saveData.testObjStack[0].testVector;
            saveData.testObjStack.RemoveAt(0);
        }
    }

    public void SaveIn(LevelData saveData)
    {
        saveData.testObjStack.Add(new TestEntitySaveData(testVector));
    }

    void Update() 
    {
        if(Input.GetButtonDown("Jump"))
        {
            testInt ++;
            testVector.x ++;
        }        
    }
}
