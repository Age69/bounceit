using System.Collections.Generic;
using UnityEngine;

public static class StaticHub 
{
    // ������� LayerMask � ��������� ��������� � int ������ �����
    // Returns a value between [0;31].
    public static int GetLayerMaskIndex(LayerMask _mask)
    {
        var bitmask = _mask.value;
        int result = bitmask > 0 ? 0 : 31;
        while (bitmask > 1)
        {
            bitmask = bitmask >> 1;
            result++;
        }
        return result;
    }

    // ������� ����� LayerMask � ������ int ������ ��������
    // Returns a list of values between [0;31].
    public static List<int> GetAllLayerMaskIndex(LayerMask _mask)
    {
        List<int> layers = new List<int>();
        var bitmask = _mask.value;
        for (int i = 0; i < 32; i++)
        {
            if (((1 << i) & bitmask) != 0)
            {
                layers.Add(i);
            }
        }
        return layers;
    }

    //��������������� ��������� � ������������ treshhold
    public static bool FastApproximately(float a, float b, float threshold)
    {
        return ((a < b) ? (b - a) : (a - b)) <= threshold;
    }

    //��������� ������ ����� �������
    public static Transform[] GetAllChilds (Transform _parent)
    {  
        int childCount = _parent.childCount;
        Transform[] result = new Transform[childCount];
        for (int i = 0; i < childCount; ++i)
            result[i] = _parent.GetChild(i);
        return result;
    }

    public static List<float> ProbabilitySumCheck(List<float> probability)
    {
        List<float> cumulativeProbability = new List<float>();
        float probabilitiesSum = 0;
        for (int i = 0; i < probability.Count; i++)
        {
            probabilitiesSum += probability[i]; //add the probability to the sum
            cumulativeProbability.Add(probabilitiesSum); //add the new sum to the list   
        }

        //All Probabilities need to be under 100% or it'll throw an exception
        if (probabilitiesSum != 100f)
        {
            Debug.LogError("Probabilities sum mastbe 100%");
            return null;
        }
        return cumulativeProbability;
    }

    //������� ������ ���������, � ����� ������ ������� ����� ������������ ������ ���� ����� 100%
    public static int GetIndexByProbability(List<float> cumulativeProbability) //[50,10,20,20]
    {
        float rnd = Random.Range(1, 101); //Get a random number between 0 and 100
        for (int i = 0; i < cumulativeProbability.Count; i++)
        {
            if (rnd <= cumulativeProbability[i]) //if the probility reach the correct sum
            {
                return i; //return the item index that has been chosen                
            }
        }
        return -1; //return -1 if some error happens
    }

    //����� ������� ���������� � ������������� ������� ����� ������������ ����� � �� ���� ����� 100% 
    public static int GetIndexByProbabilityRarity(List<float> probabilityRarity)
    {
        List<float> cumulativeByRarity = new List<float>();
        float probabilitiesSum = 0;
        float itemRaritySum = 0;

        for (int i = 0; i < probabilityRarity.Count; i++)
            itemRaritySum += probabilityRarity[i];

        float ProbilityModifier = 100 / itemRaritySum;

        for (int i = 0; i < probabilityRarity.Count; i++)
        {
            probabilitiesSum += probabilityRarity[i] * ProbilityModifier; //add the probability to the sum
            cumulativeByRarity.Add(probabilitiesSum); //add the new sum to the list
        }

        float rnd = Random.Range(1, 101); //Get a random number between 0 and 100

        for (int i = 0; i < probabilityRarity.Count; i++)
        {
            if (rnd <= cumulativeByRarity[i]) //if the probility reach the correct sum
            {
                return i; //return the item index that has been chosen 
            }
        }
        return -1; //return -1 if some error happens
    }
}
