using UnityEngine;

public class GameSettingsService : MonoBehaviour
{
    bool tutorialOn;
    public bool TutorialOn {get => tutorialOn; set => tutorialOn = value;}
    SettingsSaveService settingsSave;

    void Awake() 
    {
        settingsSave = GetComponent<SettingsSaveService>();
        LoadSettings();
    }
    public void LoadSettings()
    {
        TutorialOn = settingsSave.SettingsData.tutorialOn;
    }
    public void SaveSettings()
    {
        settingsSave.SettingsData.tutorialOn = tutorialOn;
    }
}
