using UnityEngine;
using System;
using Unity.Mathematics;

[Serializable]
public class SettingsData
{
    [HideInInspector] public int2 resolution;
    [HideInInspector] public int refreshRate, refreshRateMax;
    public float soundValue;
    public bool tutorialOn, soundOn, fullScreenOn, vSyncOn;
}
