using UnityEngine;

public class ProfileService : MonoBehaviour
{
    Profile profileCurrent;
    public Profile ProfileCurrent {get => profileCurrent;}
    ProfileData profileData;
    public ProfileData ProfileData {get => profileData;}
    ProfileSaveService profileSave;

    void Awake() 
    {
        profileSave = GetComponent<ProfileSaveService>();
        profileData = profileSave.ProfileData;
        SetSelectedProfile(profileData.selectedIndex);
    }
    public void PackProfileData()
    {
        profileSave.PackProfile();
    }
    public void StartNewProfile(string _name)
    {
        profileCurrent.profileName = _name;
        profileCurrent.newGame = false;
    }
    public void SetSelectedIndex(int _index)
    {
        profileData.selectedIndex = _index;
    }
    public void SetSelectedProfile(int _index)
    {
        profileCurrent = profileData.Profiles[_index];        
    }
    public void DeleteProfile(int _index)
    {       
        profileData.Profiles[_index] = new Profile();
    }
}
