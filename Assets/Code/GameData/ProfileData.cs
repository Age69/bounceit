using System;

[Serializable]
public class ProfileData
{
    public int selectedIndex;
    public Profile[] Profiles;
    public ProfileData()
    {
        selectedIndex = 0;
        Profiles = new Profile[3];
    }
}
