using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LevelData
{
    public List<TestEntitySaveData> testObjStack = new List<TestEntitySaveData>();

    public void ClearData()
    {
        testObjStack.Clear();
    }
}
