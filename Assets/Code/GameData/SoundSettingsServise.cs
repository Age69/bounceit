using UnityEngine;

public class SoundSettingsServise : MonoBehaviour
{
    bool soundOn;
    public bool SoundOn 
    {
        get => soundOn; 
        set 
        {
            soundOn = value;
            if(SoundOn)
                AudioListener.volume = soundValue;
            else
                AudioListener.volume = 0;
        }
    }
    float soundValue;
    public float SoundValue 
    {
        get => soundValue; 
        set 
        {
            soundValue = value;
            AudioListener.volume = soundValue;
        }
    }
    SettingsSaveService settingsSave;

    void Awake() 
    {
        settingsSave = GetComponent<SettingsSaveService>();
        LoadSettings();
    }
    public void LoadSettings()
    {
        SoundOn = settingsSave.SettingsData.soundOn;
        SoundValue = settingsSave.SettingsData.soundValue;
    }
    public void SaveSettings()
    {
        settingsSave.SettingsData.soundOn = soundOn;
        settingsSave.SettingsData.soundValue = soundValue;
    }
}
