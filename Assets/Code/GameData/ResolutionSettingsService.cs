using UnityEngine;
using Unity.Mathematics;

public class ResolutionSettingsService : MonoBehaviour
{
    int currentIndex, selectedIndex, maxIndex, currentRefreshRate;
    int2[] resolutions;
    int refreshRate;
    public int RefreshRate {get => refreshRate;}
    int refreshRateMax;
    public int RefreshRateMax {get => refreshRateMax;}
    bool fullScreen;
    public bool FullScreen 
    {
        get => fullScreen; 
        set
        {
            fullScreen = value;
            Screen.fullScreen = fullScreen;
        }
    }
    bool vSync;
    public bool VSync 
    {
        get => vSync; 
        set 
        {
            vSync = value;
            if(vSync)
            QualitySettings.vSyncCount = 1;            
            else
            QualitySettings.vSyncCount = 0;
        }
    }    
    SettingsSaveService settingsSave;

    void Awake() 
    {
        settingsSave = GetComponent<SettingsSaveService>();
        SetSettingsTune();
    }
    void SetSettingsTune()
    {
        //create resolutions array
        Resolution[] monitorRes = Screen.resolutions;
        resolutions = new int2[monitorRes.Length];
        int count = monitorRes.Length;
        for(int i = 0; i < count; i ++)
        {
            resolutions[i].x = monitorRes[i].width;
            resolutions[i].y = monitorRes[i].height;
        }
        maxIndex = resolutions.Length -1;

        //find index of installed res in resolutions array
        //if not, check installed res for unsupported monitor
        currentIndex = -1;
        count = resolutions.Length;
        for(int i = 0; i < count; i ++)
        {
            if(settingsSave.SettingsData.resolution.Equals(resolutions[i]))
            {
                currentIndex = i;
                break;
            }
        }                              
        if(currentIndex < 0)
        {
            currentIndex = resolutions.Length -1;
            settingsSave.SettingsData.resolution = resolutions[currentIndex];
        }   
        LoadSettings();        
    }
    public int2 ReturnToCurrentResolution()
    {
        selectedIndex = currentIndex;
        refreshRate = currentRefreshRate;
        SetSelectedResolution();
        return resolutions[currentIndex];
    }
    public int2 ApplySelectedResolution()
    {
        currentIndex = selectedIndex;
        currentRefreshRate = refreshRate;
        return resolutions[currentIndex];
    }
    public void ChangeRefrashRate(int _value, ref bool _aplly)
    {
        refreshRate = _value;
        _aplly = currentRefreshRate != refreshRate;
    }
    public void SetSelectedResolution()
    {
        Screen.SetResolution(resolutions[selectedIndex].x, 
            resolutions[selectedIndex].y, fullScreen, refreshRate);
    }
    public int2 ChangeResolution(int _direction, ref bool _aplly)
    {
        selectedIndex += _direction;
        if(selectedIndex > maxIndex )
            selectedIndex = 0;
        if(selectedIndex < 0)
            selectedIndex = maxIndex;
        _aplly = currentIndex != selectedIndex;
        return resolutions[selectedIndex];
    }
    public void LoadSettings()
    {
        VSync = settingsSave.SettingsData.vSyncOn;

        fullScreen = settingsSave.SettingsData.fullScreenOn;
        refreshRate = settingsSave.SettingsData.refreshRate;
        refreshRateMax = settingsSave.SettingsData.refreshRateMax;
        selectedIndex = currentIndex;
        currentRefreshRate = refreshRate;      
        SetSelectedResolution();
    }
    public void SaveSettings()
    {
        settingsSave.SettingsData.resolution = resolutions[currentIndex];
        settingsSave.SettingsData.refreshRate = refreshRate;
        settingsSave.SettingsData.fullScreenOn = fullScreen;
        settingsSave.SettingsData.vSyncOn = vSync;
    }
}
