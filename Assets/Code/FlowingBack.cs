using UnityEngine;
using UnityEngine.UI;

public class FlowingBack : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    RawImage back;
    void Start()
    {
        back = GetComponent<RawImage>();
    }

    void FixedUpdate()
    {
        back.uvRect = new Rect(back.uvRect.x + moveSpeed * Time.deltaTime, back.uvRect.y + moveSpeed * Time.deltaTime, 1f, 1f);
    }
}
