﻿using System.Collections.Generic;
using UnityEngine;

public class GlobalFixUpdateSystem : MonoBehaviour
{
    List<IDoInFixUpdate> fixUpdateObjects = new List<IDoInFixUpdate>();
    public List<IDoInFixUpdate> FixUpdateObjects { get { return fixUpdateObjects; } }

    void FixedUpdate()
    {
        var count = fixUpdateObjects.Count;
        for (var i = 0; i < count; i++) fixUpdateObjects[i].DoInFixUpdate();
    }
}