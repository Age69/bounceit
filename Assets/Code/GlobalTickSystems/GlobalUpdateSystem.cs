using System.Collections.Generic;
using UnityEngine;

public class GlobalUpdateSystem : MonoBehaviour
{
    List<IDoInUpdate> updateObjects = new List<IDoInUpdate>();
    public List<IDoInUpdate> UpdateObjects { get { return updateObjects; } }
    
    void Update()
    {
        var count = updateObjects.Count;
        for (var i = 0; i < count; i++) updateObjects[i].DoInUpdate();
    }
}
