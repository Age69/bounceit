﻿using System.Collections.Generic;
using UnityEngine;

public abstract class GenericObjectPool<T> : MonoBehaviour where T : Component
{
    // Т это класс обьекта который и будет спамиться, под него весь пул делаеться

    [SerializeField] private T prefab;//префаб обьекта из пулла
    [SerializeField] private int startObjectsCount;//колво создаваемых в пулл обьектов со старта
    private Queue<T> objects = new Queue<T>();//все обьекты тусят в очереди

    //достаем обьект из пулла
    public T GetFromPool()
    {
        T newObject;
        //либо обькт содаеться, либо реюзается
        if (objects.Count == 0)
            newObject = GameObject.Instantiate(prefab);
        else
            newObject = objects.Dequeue();

        return newObject;
    }

    //возвращаем обьект в пулл
    public void ReturnToPool (T returnObject)
    {
        objects.Enqueue(returnObject);
    }

    //загружаем начальный пулл обьектов
    public void AddStartObjects ()
    {
        for (int i = 0; i < startObjectsCount; i++)
        {
            var newObject = GameObject.Instantiate(prefab);
            newObject.gameObject.SetActive(false);
            objects.Enqueue(newObject);
        }
    }
}
