using UnityEngine;

public class PoolRefManager : MonoBehaviour
{
    [SerializeField] EnemyPoolRandomizer enemyPoolRandomizer;

    public EnemyPoolRandomizer EnemyPoolRandomizer { get { return enemyPoolRandomizer; } }

    void Start()
    {
        PoolsStartObjects();
    }

    void PoolsStartObjects()
    {
        // particlePool.AddStartObjects();
    }
}
