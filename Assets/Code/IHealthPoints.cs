
public interface IHealthPoints 
{
    void Damage(int dmg);
    void Death();
}
